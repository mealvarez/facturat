var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    path = require('path'),
    mongoose = require('mongoose'),
    bodyParser = require("body-parser");


//server.listen(8000); esto es fuera de cloud9
server.listen(process.env.PORT, process.env.IP);
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1/data');

//Sirve archivos estaticos, en este casi la carpeta public
app.use(express.static(path.resolve(__dirname, 'public')));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "X-Requested-With");
   res.header('Access-Control-Allow-Headers', 'Content-Type');
   next(); // make sure we go to the next routes and don't stop here
});



//Model of Moongose -DataBase
var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;

var facturSchema = new Schema({
  id        : ObjectId,
  amount    : { type: String, default: 0.0 },
  name      : { type: String, required: true, unique: true },
  payDate      : { type: Date, default: Date.now },
});

facturSchema.pre('save', function(next) {
  // get the current date
  this.payDate = new Date();
  
  next();
});

var Factur = mongoose.model('Factur', facturSchema);
module.exports = Factur;


app.get('/', function(req, res) {
   res.sendFile(__dirname + '/index.html'); 
});

app.get('/index', function(req, res) {
   res.sendFile(__dirname + '/index.html'); 
});


//enrutamos para realizar codeo dinamico
app.route('/facturs')
  .get(function(req, res) {
      Factur.find({}, function(err, facturs) {
         if (err) throw err;
         
            /*   objetos que devuelven 
                  {  
                     _id: 58d57123544db61a7d5dd3f5,
                      name: 'edesur',
                      __v: 0,
                      payDate: Fri Mar 24 2017 19:18:59 GMT+0000 (UTC),
                      amount: '11' 
                  },
            */
            
            return res.send(facturs);
      });
  })
  .post(function(req, res) {
      var newFactur = Factur({
         name   : req.body.name,
         amount : req.body.amount
      });
    
      newFactur.save(function(err, save) {
         if (err) {
            console.log(err);
            return res.send({
               message: 'something went wrong'
            });
         } else {
            return res.send({
              message: 'the Factur has bees saved',
              deudasave : save
            });
         }
         
      });
  })
  .put(function(req, res) {
      Factur.findById(1, function(err, user) {
         if (err) throw err;
      
         // change the users location
         user.location = 'uk';
      
         // save the user
         user.save(function(err) {
            if (err) throw err;
               console.log('User successfully updated!');
         });

      });
     
      res.send('Update a factur ' );
  });
  
app.route("/facturs/:id")
  .delete(function(req, res) {
      Factur.remove({
         _id: req.params.id
      }, function(err, bear) {
         if (err)
             return res.send(err);
             
         res.json({ deleted_id: req.params.id, message: 'Successfully deleted' });
     });
   });   