var factursList = [];

$(function(){
    $('totalamount').val('0');
    //recuperar datos almacenados y mostrar lista factur
    startLoad();
    
    $('#add').click(function(e) {
        if( !isEmptyOrSpaces($('#amount').val()) && 
            !isEmptyOrSpaces($('#amount').val()) ) {
            var deuda = {
                'amount' : $('#amount').val(),
                'name' : $('#name').val()
            };
            
            //ajax para guardar en el servidor
            save(deuda);
        } else {
            alert("completa todo gato");
        }
    });
    
    $("input").keyup(function(event){
    if(event.keyCode == 13){
        $("#add").click();
    }
});
    
});

function startLoad(){
    $.ajax({
	    type        : "GET",
	    url         : "facturs",
        datatype    : "json",
        contentyype : "application/json",
        success     : function(result) {
            var total = parseInt($('#totalamount').text());
            
            for (var i = 0; i < result.length; i++) {
                var id = result[i]._id;
                $('#facturs').append('<li id="'+ id +'" class="list-group-item element"></li>');
                
                $('#' + id).append('<span>' + result[i].name + ' - ' + result[i].amount + '</span>');
                $('#' + id).append('<button class="btnDelete btn btn-danger" onclick="deleteFactur(this)"  type="submit" > <i class="fa fa-trash-o" aria-hidden=true></i></button>');
                
                total = total + parseInt(result[i].amount);
                factursList.push(result[i]);
            }
            
            $('#content').append(result);
            $('#totalamount').text(total);
            
        }
    });
}


function save(deuda){
	$.ajax({
	    type        : "POST",
	    url         : "facturs",
        datatype    : "json",
        data        : deuda,
        contentyype : "application/json",
        success     : function(result) {
            console.log(result);
            if(result.message) {
                $('.alert').text(result.message).fadeIn(400).delay(3000).fadeOut(400);
                
                $('#facturs').append('<li id="'+ result.deudasave._id +'" class="list-group-item element"></li>');
                
                $('#' + result.deudasave._id).append('<span>' + result.deudasave.name + ' - ' + result.deudasave.amount + '</span>');
                $('#' + result.deudasave._id).append('<button class="btnDelete btn btn-danger" onclick="deleteFactur(this)"> <i class="fa fa-trash-o" aria-hidden=true></i></button>');
                
                //agregar a la lista
                factursList.push(result.deudasave);
                calculateTotal(result.deudasave._id);
                
                $('#amount').val("");
                $('#name').val("");
            }
        }
    });
};

function deleteFactur(me){
    var id = me.parentElement.id;
    $.ajax({
        type        : "DELETE",
	    url         : "facturs/" + id,
        success     : function(result) {
            
            if(result.message){
                $('.alert').text(result.message).fadeIn(400).delay(3000).fadeOut(400);
                
                calculateTotal(result.deleted_id, true);
                factursList = factursList.filter(item => item._id !== result.deleted_id);
                
                
                
                $('#' + result.deleted_id).remove();   
            }
        }
    })
}

function calculateTotal(id, resta = false) {
    var total = parseInt($('#totalamount').text());
    var result = $.grep(factursList, function(e){ return e._id == id; });
    var amount = parseInt(result[0].amount);
    total = resta ? total - amount : total + amount; 
    $('#totalamount').text(total);
}


function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}